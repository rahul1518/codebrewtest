"use strict";
const HELPERS = require("../../helpers");
const { MESSAGES, ERROR_TYPES, NORMAL_PROJECTION, USER_ROLES, BOOKING_STATUS, MAX_FILE_SIZE } = require('../../utils/constants');
const SERVICES = require('../../services');
const { compareHash, encryptJwt } = require(`../../utils/utils`);
const CONFIG = require('../../../config');
const path = require('path');


/**************************************************
 ***** Auth controller for authentication logic ***
 **************************************************/
let userController = {};

/**
 * function to register a user to the system.
 */
userController.registerNewUser = async (payload) => {
  let isUserAlreadyExists = await SERVICES.userService.getUser({ email: payload.email, role: USER_ROLES.USER, isDeleted: false, isBlocked: false }, NORMAL_PROJECTION);
  if (!isUserAlreadyExists) {
    payload.role = USER_ROLES.USER;
    let newRegisteredUser = await SERVICES.userService.registerUser(payload);
    return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.USER_REGISTERED_SUCCESSFULLY), { user: newRegisteredUser });
  }
  throw HELPERS.responseHelper.createErrorResponse(MESSAGES.EMAIL_ALREADY_EXISTS, ERROR_TYPES.BAD_REQUEST);
};

/**
 * function to login a user to the system.
 */
userController.loginUser = async (payload) => {
  // check is user exists in the database with provided email or not.
  let user = await SERVICES.userService.getUser({ email: payload.email, role: payload.role, isBlocked: false, isDeleted: 0 }, NORMAL_PROJECTION);
  // if user exists then compare the password that user entered.
  if (user) {
    // compare user's password.
    if (compareHash(payload.password, user.password)) {
      const dataForJwt = {
        id: user._id,
        date: Date.now()
      };
      return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.LOGGED_IN_SUCCESSFULLY), { token: encryptJwt(dataForJwt) });
    }
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.INVALID_PASSWORD, ERROR_TYPES.BAD_REQUEST);
  }
  throw HELPERS.responseHelper.createErrorResponse(MESSAGES.INVALID_EMAIL, ERROR_TYPES.BAD_REQUEST);
};

//Get nearby fuel stations.
userController.getNearByPumps = async (payload) => {
  let stations = await SERVICES.userService.getNearByPumps(payload);
  return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.NEAR_BY_FUEL_STATIONS), { stationsInfos: stations });
};

//Function to create new request.
userController.registerNewRequest = async (payload) => {
  //check is vaild station id.
  let isStationExists = await SERVICES.userService.getUser({ _id: payload.stationId, role: USER_ROLES.STATION_OWNER, isDeleted: false, isBlocked: false }, {});
  if (!isStationExists) {
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.INVALID_STATION_OWNER_ID, ERROR_TYPES.BAD_REQUEST);
  }
  payload.customerId = payload.user._id;
  let newRequest = await SERVICES.bookingService.addBooking(payload);
  return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.NEAR_BY_FUEL_STATIONS), { requestInfo: newRequest });
};

//Function to verify to mark the request is completed.
userController.completeRequest = async (payload) => {
  //get request info.
  let requestInfo = await SERVICES.bookingService.getBookingInfo({ _id: payload.bookingId }, { status: 1 }, { lean: true });
  if (!requestInfo) {
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.INVALID_BOOKING_ID, ERROR_TYPES.BAD_REQUEST);
  }
  if (!requestInfo.status === BOOKING_STATUS.REQUESTED) {
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.REQUEST_NOT_MARK_COMPLETED_BY_STATION, ERROR_TYPES.BAD_REQUEST);
  }
  if (!requestInfo.status === BOOKING_STATUS.CONFIRM_BY_CUSTOMER) {
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.ALREADY_COMPLETED, ERROR_TYPES.BAD_REQUEST);
  }
  await SERVICES.bookingService.updateBooking({ _id: payload.bookingId }, { status: BOOKING_STATUS.CONFIRM_BY_CUSTOMER }, { lean: true, new: true });
  return HELPERS.responseHelper.createSuccessResponse(MESSAGES.REQUEST_MARKED_COMPLETED);
};

//Function to mark complete request by fuel station.
userController.requestMarkCompletedByPump = async (payload) => {
  let updatedRequest = await SERVICES.bookingService.updateBooking({ stationId: payload.user._id, status: BOOKING_STATUS.REQUESTED }, { status: BOOKING_STATUS.CONFIRMED_BY_STATION }, { lean: true });
  if (!updatedRequest) {
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.INVALID_BOOKING_ID, ERROR_TYPES.BAD_REQUEST);
  }
  return HELPERS.responseHelper.createSuccessResponse(MESSAGES.REQUEST_MARKED_COMPLETED);
};

/**
 * Function to upload file.
 */
userController.uploadFile = async (payload) => {
  if (payload.file.size > MAX_FILE_SIZE) {
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.FILE_SIZE_GREATER_THAN_3_MB, ERROR_TYPES.BAD_REQUEST);
  }
  // check whether the request contains valid payload.
  if (!Object.keys(payload.file).length) {
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.FILE_REQUIRED_IN_PAYLOAD, ERROR_TYPES.BAD_REQUEST);
  }
  let pathToUpload = path.resolve(__dirname + `../../../..${CONFIG.PATH_TO_UPLOAD_FILES_ON_LOCAL}`),
    pathOnServer = CONFIG.PATH_TO_UPLOAD_FILES_ON_LOCAL;
  let fileUrl = await SERVICES.fileUploadService.uploadFile(payload, pathToUpload, pathOnServer);
  return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.FILE_UPLOADED_SUCCESSFULLY), { fileUrl });
};

/**
 * Function get station request status.
 */
userController.getBookings = async (payload) => {
  let bookings = await SERVICES.userService.getBookings(payload);
  return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.SUCCESSFULLY_FETCHED), { bookings });
};

/* export userController */
module.exports = userController;