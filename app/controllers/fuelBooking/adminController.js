"use strict";
const HELPERS = require("../../helpers");
const { compareHash, encryptJwt } = require('../../utils/utils');
const { MESSAGES, ERROR_TYPES, NORMAL_PROJECTION, USER_ROLES } = require('../../utils/constants');
const SERVICES = require('../../services');

let adminController = {};

adminController.login = async (payload) => {
    let criteria = {
        email: payload.email
    };
    let admin = await SERVICES.adminService.adminLogin(criteria, NORMAL_PROJECTION);
    if (admin) {
        if (compareHash(payload.password, admin.password)) {
            //create jwt token and send it in the response.
            let dataForJWT = {
                _id: admin._id,
                Date: Date.now,
                email: admin.email
            };
            delete admin.password;
            return Object.assign(HELPERS.responseHelper.createSuccessResponse(MESSAGES.LOGGED_IN_SUCCESSFULLY), {
                data: admin,
                token: encryptJwt(dataForJWT)
            });
        }
        throw HELPERS.responseHelper.createErrorResponse(MESSAGES.INVALID_PASSWORD, ERROR_TYPES.BAD_REQUEST);
    }
    throw HELPERS.responseHelper.createErrorResponse(MESSAGES.INVALID_CREDENTIALS, ERROR_TYPES.BAD_REQUEST);
};

//Function to add station.
adminController.addStation = async (payload) => {
    //check is station owner already added or not.
    let isStationAlreadyAdded = await SERVICES.userService.getUser({ email: payload.email, role: USER_ROLES.STATION_OWNER });
    if (isStationAlreadyAdded) {
        return HELPERS.responseHelper.createErrorResponse(MESSAGES.STATION_ALREADY_EXISTS_WITH_THIS_EMAIL, ERROR_TYPES.BAD_REQUEST);
    }
    await SERVICES.userService.registerUser({ ...payload, role: USER_ROLES.STATION_OWNER });
    return HELPERS.responseHelper.createSuccessResponse(MESSAGES.STATION_ADDED_SUCCESSFULLY);
};

/* export adminController */
module.exports = adminController;