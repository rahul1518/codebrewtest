
'use strict';


/********************************
 ********* All routes ***********
 ********************************/
let v1Routes = [
    ...require('./userRoutes'),
    ...require(`./adminRoutes`)
]
module.exports = v1Routes;
