'use strict';

const Joi = require('joi');
const CONFIG = require('../../../../config');
const { AVAILABLE_AUTHS, FUEL_TYPES } = require(`../../../utils/constants`);
//load controllers
const { registerNewUser, loginUser, getNearByPumps, registerNewRequest, requestMarkCompletedByPump, completeRequest, uploadFile, getBookings } = require(`../../../controllers/${CONFIG.PLATFORM}/userController`);

let routes = [
	{
		method: 'POST',
		path: '/v1/user/register',
		joiSchemaForSwagger: {
			body: Joi.object({
				email: Joi.string().email().required().description('User\'s email.'),
				password: Joi.string().required().description('User\'s password.'),
				name: Joi.string().required().description('User\'s name.'),
				country: Joi.string().required().description('User\'s country.'),
				city: Joi.string().required().description('User\'s city.')
			}).unknown(),
			group: 'User',
			description: 'Route to register a user.',
			model: 'Register'
		},
		handler: registerNewUser
	},
	{
		method: 'POST',
		path: '/v1/user/login',
		joiSchemaForSwagger: {
			body: Joi.object({
				email: Joi.string().email().required().description('User\'s email Id.'),
				password: Joi.string().required().description('User\'s password.'),
				role: Joi.number().allow(1, 2).required().description('1 for user, 2 for station owner.')
			}).unknown(),
			group: 'User',
			description: 'Route to login a user.',
			model: 'Login'
		},
		auth: false,
		handler: loginUser
	},
	{
		method: 'GET',
		path: '/v1/user/nearPumps',
		joiSchemaForSwagger: {
			headers: Joi.object({
				'authorization': Joi.string().required().description('Users\'s JWT token.')
			}).unknown(),
			query: Joi.object({
				location: Joi.array().items(
					Joi.number()
				).min(2).max(2).description('Coordinates of location.'),
				requiredFuels: Joi.array().items(
					Joi.number().allow(1, 2, 3)
				).min(1).description('Required fuels. 1 for petrol,2 for diesel, 3 for cng'),
				skip: Joi.number().optional().description('Number of documents to skip.'),
				limit: Joi.number().optional().description('Number of documents to get.')
			}).unknown(),
			group: 'User',
			description: 'Route to get near by pumps.',
			model: 'NEAR_PUMPS'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: getNearByPumps
	},
	{
		method: 'POST',
		path: '/v1/user/registerRequest',
		joiSchemaForSwagger: {
			headers: Joi.object({
				'authorization': Joi.string().required().description('Users\'s JWT token.')
			}).unknown(),
			body: Joi.object({
				vehicles: Joi.array().items(Joi.object({
					vehicleNumber: Joi.string().required().description('Vehicle number.'),
					fuelType: Joi.number().allow(FUEL_TYPES.DIESEL, FUEL_TYPES.PETROL, FUEL_TYPES.CNG).required().description('Vehicle fuel type.')
				}).required().description('Vehiccles infos.')),
				stationId: Joi.string().required().description('Fuel station id.')
			}).unknown(),
			group: 'User',
			description: 'Route to register fuel filling request.',
			model: 'REGISTER_REQUEST'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: registerNewRequest
	},
	{
		method: 'POST',
		path: '/v1/user/markRequestCompleted',
		joiSchemaForSwagger: {
			headers: Joi.object({
				'authorization': Joi.string().required().description('Users\'s JWT token.')
			}).unknown(),
			body: Joi.object({
				bookingId: Joi.string().required().description('Booking id.')
			}).unknown(),
			group: 'User',
			description: 'Route to mark complete fuel filling request.',
			model: 'COMPLETE_REQUEST'
		},
		auth: AVAILABLE_AUTHS.USER,
		handler: completeRequest
	},
	{
		method: 'POST',
		path: '/v1/fuelStation/markRequestCompleted',
		joiSchemaForSwagger: {
			headers: Joi.object({
				'authorization': Joi.string().required().description('Fuel Station\'s JWT token.')
			}).unknown(),
			body: Joi.object({
				bookingId: Joi.string().required().description('Booking id.')
			}).unknown(),
			group: 'FUEL_STATION',
			description: 'Route to mark complete fuel filling request.',
			model: 'COMPLETE_REQUEST'
		},
		auth: AVAILABLE_AUTHS.FUEL_STATION,
		handler: requestMarkCompletedByPump
	},
	{
		method: 'POST',
		path: '/v1/uploadFile',
		joiSchemaForSwagger: {
			formData: {
				file: Joi.any().meta({ swaggerType: 'file' }).optional().description('Image File.')
			},
			group: 'File',
			description: 'Route to upload a file.',
			model: 'FILE_UPLOAD'
		},
		requestTimeout: true,
		handler: uploadFile
	}, {
		method: 'GET',
		path: '/v1/fuel/bookings',
		joiSchemaForSwagger: {
			headers: Joi.object({
				'authorization': Joi.string().required().description('Fuel Station\'s JWT token.')
			}).unknown(),
			query: Joi.object({
				skip: Joi.number().optional().description('Number of documents to skip.'),
				limit: Joi.number().optional().description('Number of documents to get.'),
				status: Joi.number().allow(1, 2, 3).required().default(1).description('1 for pending requests, 2 for complted by pump not confirmed by user, 3 completed')
			}).unknown(),
			group: 'FUEL_STATION',
			description: 'Route to get bookings.',
			model: 'GET_BOOKINGS'
		},
		auth: AVAILABLE_AUTHS.FUEL_STATION,
		handler: getBookings
	}
];

module.exports = routes;




