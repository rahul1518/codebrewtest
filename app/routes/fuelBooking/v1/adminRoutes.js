'use strict';

const Joi = require('joi');
const CONFIG = require('../../../../config');
const { AVAILABLE_AUTHS } = require('../../../utils/constants');
// load controllers
const { login, addStation } = require(`../../../controllers/${CONFIG.PLATFORM}/adminController`);


let routes = [
    {
        method: 'POST',
        path: '/v1/admin/login',
        joiSchemaForSwagger: {
            body: {
                email: Joi.string().email().required().description('Admin email address.'),
                password: Joi.string().required().description('Admin password.')
            },
            group: 'Admin',
            description: 'Route to login an admin.',
            model: 'ADMIN_LOGIN'
        },
        auth: false,
        handler: login
    },
    {
        method: 'POST',
        path: '/v1/admin/addStation',
        joiSchemaForSwagger: {
            headers: Joi.object({
                'authorization': Joi.string().required().description('Admin\'s JWT token.')
            }).unknown(),
            body: {
                email: Joi.string().required().description('Fuel station Email address.'),
                password: Joi.string().required().description('Fuel Station password.'),
                city: Joi.string().required().description('Fuel Station city.'),
                country: Joi.string().required().description('Fuel Station country.'),
                fuelStationLocation: {
                    type: Joi.string().required().valid('Point').description('Type of location.'),
                    coordinates: Joi.array().items(
                        Joi.number()
                    ).min(2).max(2).description('Coordinates of location.')
                },
                availableFuels: Joi.array().unique().items(
                    Joi.number().allow(1, 2, 3)
                ).min(1).description('Required fuels. 1 for petrol,2 for diesel, 3 for cng'),
            },
            group: 'Admin',
            description: 'Route to add station by an admin.',
            model: 'ADD_STATION'
        },
        auth: AVAILABLE_AUTHS.ADMIN,
        handler: addStation
    }
];

module.exports = routes;