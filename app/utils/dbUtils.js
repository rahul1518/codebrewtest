const MODELS = require('../models/index');
const { hashPassword } = require(`../utils/utils`);

let dbUtils = {};

/**
 * function to create initial admin. 
 */
dbUtils.createAdmin = async () => {
  let isAdminExists = await MODELS.adminModel.countDocuments();
  let adminDetails = {
    email: process.env.ADMIN_EMAIL || 'admin@admin.com',
    password: hashPassword(process.env.ADMIN_PASSWORD || 'admin')
  }
  if (!isAdminExists) {
    await MODELS.adminModel(adminDetails).save();
  }
  if (isAdminExists) {
    await MODELS.adminModel.findOneAndUpdate({ email: adminDetails.email }, { $set: { password: adminDetails.password } });
  }
};


module.exports = dbUtils;