const jwt = require('jsonwebtoken');

const { SECURITY, MESSAGES, ERROR_TYPES, USER_ROLES } = require('../../utils/constants');
const HELPERS = require("../../helpers");
const { userModel, adminModel } = require(`../../models`);

let authService = {};

/**
 * function to authenticate user.
 */
authService.userValidate = () => {
    return (request, response, next) => {
        validateUser(request).then((isAuthorized) => {
            if (isAuthorized) {
                return next();
            }
            let responseObject = HELPERS.responseHelper.createErrorResponse(MESSAGES.UNAUTHORIZED, ERROR_TYPES.UNAUTHORIZED);
            return response.status(responseObject.statusCode).json(responseObject);
        }).catch((err) => {
            let responseObject = HELPERS.responseHelper.createErrorResponse(MESSAGES.UNAUTHORIZED, ERROR_TYPES.UNAUTHORIZED);
            return response.status(responseObject.statusCode).json(responseObject);
        });
    };
};


/**
 * function to validate user's jwt token and fetch its details from the system. 
 * @param {} request 
 */
let validateUser = async (request) => {
    try {
        let decodedToken = jwt.verify(request.headers.authorization, SECURITY.JWT_SIGN_KEY);
        let authenticatedUser = await userModel.findOne({ _id: decodedToken.id, role: USER_ROLES.USER }).lean();
        if (authenticatedUser) {
            request.user = authenticatedUser;
            return true;
        }
        return false;
    } catch (err) {
        return false;
    }
};

/**
 * function to authenticate user.
 */
authService.fuelStationValidate = () => {
    return (request, response, next) => {
        validateFuelStation(request).then((isAuthorized) => {
            if (isAuthorized) {
                return next();
            }
            let responseObject = HELPERS.responseHelper.createErrorResponse(MESSAGES.UNAUTHORIZED, ERROR_TYPES.UNAUTHORIZED);
            return response.status(responseObject.statusCode).json(responseObject);
        }).catch((err) => {
            let responseObject = HELPERS.responseHelper.createErrorResponse(MESSAGES.UNAUTHORIZED, ERROR_TYPES.UNAUTHORIZED);
            return response.status(responseObject.statusCode).json(responseObject);
        });
    };
};


/**
 * function to validate user's jwt token and fetch its details from the system. 
 * @param {} request 
 */
let validateFuelStation = async (request) => {
    try {
        let decodedToken = jwt.verify(request.headers.authorization, SECURITY.JWT_SIGN_KEY);
        let authenticatedUser = await userModel.findOne({ _id: decodedToken.id, role: USER_ROLES.STATION_OWNER }).lean();
        if (authenticatedUser) {
            request.user = authenticatedUser;
            return true;
        }
        return false;
    } catch (err) {
        return false;
    }
};

/**
 * function to validate user's token from samsung server if it is valid or not.
 */
authService.validateToken = async (token) => {
    let isValidToken = true;
    return isValidToken;
};

/**
 * function to authenticate admin.
 */
authService.adminValidate = () => {
    return (request, response, next) => {
        validateAdmin(request).then((isAuthorized) => {
            if (isAuthorized) {
                return next();
            }
            let responseObject = HELPERS.responseHelper.createErrorResponse(MESSAGES.UNAUTHORIZED, ERROR_TYPES.UNAUTHORIZED);
            return response.status(responseObject.statusCode).json(responseObject);
        }).catch((err) => {
            let responseObject = HELPERS.responseHelper.createErrorResponse(MESSAGES.UNAUTHORIZED, ERROR_TYPES.UNAUTHORIZED);
            return response.status(responseObject.statusCode).json(responseObject);
        });
    };
};

/**
 * function to validate an admin.
 */
let validateAdmin = async (request) => {
    try {
        let authorizedAdmin = jwt.verify(request.headers.authorization, SECURITY.JWT_SIGN_KEY);
        let authenticatedAdmin = await adminModel.findById(authorizedAdmin._id).lean();
        if (authenticatedAdmin) {
            request.admin = authenticatedAdmin;
            return true;
        }
        return false;
    } catch (err) {
        return false;
    }
};

module.exports = authService;