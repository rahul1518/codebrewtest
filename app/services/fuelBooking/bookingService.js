const { MESSAGES, ERROR_TYPES } = require('../../utils/constants');
const CONFIG = require('../../../config');
const HELPERS = require("../../helpers");
const bookingModel = require(`../../models/${CONFIG.PLATFORM}/bookingsModel`);

let bookingService = {};

/**
 * function to add booking.
 */
bookingService.addBooking = async (dataToSave) => {
    return await bookingModel(dataToSave).save();
};

/**
 * function to get booking info.
 */
bookingService.getBookingInfo = async (criteria, projection, options) => {
    return await bookingModel.findOne(criteria, projection, options);
};

/**
 * function to update booking.
 */
bookingService.updateBooking = async (criteria, dataToUpdate, options) => {
    return await bookingModel.findOneAndUpdate(criteria, dataToUpdate, options);
};
module.exports = bookingService;