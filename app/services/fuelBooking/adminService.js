const { SECURITY, MESSAGES, ERROR_TYPES } = require('../../utils/constants');
const CONFIG = require('../../../config');
const HELPERS = require("../../helpers");
const adminModel = require(`../../models/${CONFIG.PLATFORM}/adminModel`);

let adminService = {};

/**
 * function to login an admin in to the system.
 */
adminService.adminLogin = async (criteria, projection) => {
    return await adminModel.findOne(criteria, projection).lean();
};

module.exports = adminService;