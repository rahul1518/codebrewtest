'use strict';
const { userModel, bookingModel } = require(`../../models`);
const utils = require(`../../utils/utils`);
const { USER_ROLES, PAGINATION } = require('../../utils/constants');

let userService = {};

/** 
 * function to register a new  user
 */
userService.registerUser = async (payload) => {
  // encrypt user's password and store it in the database.
  payload.password = utils.hashPassword(payload.password);
  return await userModel(payload).save();
};

/**
 * function to update user.
 */
userService.updateUser = async (criteria, dataToUpdate, projection = {}) => {
  let userData = await userService.getUser(criteria);
  let updatedUserData = await userModel.findOneAndUpdate(criteria, dataToUpdate, { new: true, projection: projection }).lean();
  //function to maintain the users stats history.
  await userService.updateUserStatsHistory(userData, updatedUserData);
  return updatedUserData;
};

/**
 * function to fetch user from the system based on criteria.
 */
userService.getUser = async (criteria, projection) => {
  return await userModel.findOne(criteria, projection).lean();
};

/**
 * function to create new user into the system.
 */
userService.createUser = async (payload) => {
  return await userModel(payload).save();
};

/**
 * Function to get near by pumps.
 */
userService.getNearByPumps = async (payload) => {
  let skip = payload.skip || PAGINATION.DEFAULT_NUMBER_OF_DOCUMENTS_TO_SKIP;
  let limit = payload.limit || PAGINATION.DEFAULT_LIMIT;
  let query = [
    {
      $geoNear: {
        near: { type: "Point", coordinates: payload.location },
        distanceField: "distance",
        spherical: true,
        query: { role: USER_ROLES.STATION_OWNER, isDeleted: false, isBlocked: false }
      }
    },
    {
      $addFields: {
        isFuelAvailables: { $setIsSubset: [payload.requiredFuels, '$availableFuels'] }
      }
    },
    {
      $sort: { isFuelAvailables: -1, distance: 1 }
    },
    {
      $skip: skip
    },
    {
      $limit: limit
    },
    {
      $project: {
        __v: 0,
        createdAt: 0,
        updatedAt: 0,
        password: 0, isFuelAvailables: 0
      }
    }
  ];
  return await userModel.aggregate(query);
};

//Function to get bookings.
userService.getBookings = async (payload) => {
  let limit = payload.limit || PAGINATION.DEFAULT_LIMIT, skip = payload.skip || PAGINATION.DEFAULT_NUMBER_OF_DOCUMENTS_TO_SKIP;
  let criteria = { stationId: payload.user._id, status: payload.status };
  let query = [
    { $match: { ...criteria } },
    {
      $lookup: {
        from: "users",
        let: { "is_blocked": false, "is_deleted": false, "user_id": '$customerId' },
        pipeline: [
          {
            $match: {
              $expr: { $and: [{ $eq: ['$isBlocked', '$$is_blocked'] }, { $and: [{ $eq: ['$isDeleted', '$$is_deleted'] }, { $eq: ['$_id', '$$user_id'] }] }] }
            }
          },
          { $project: { createdAt: 0, updatedAt: 0, __v: 0, _id: 0, password: 0, isBlocked: 0, isDeleted: 0, availableFuels: 0, fuelStationLocation: 0, role: 0 } }
        ],
        as: "customerInfo"
      }
    },
    {
      $unwind: { path: '$customerInfo', "preserveNullAndEmptyArrays": true }
    },
    {
      $project: {
        __v: 0,
        updatedAt: 0,
        createdAt: 0,
        'customerInfo.password': 0,
        'customerInfo.password': 0,
      }
    },
    { $skip: skip },
    { $limit: limit }
  ];
  return await bookingModel.aggregate(query);
};

module.exports = userService;