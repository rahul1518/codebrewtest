
const CONFIG = require('../../config');
/********************************
 **** Managing all the services ***
 ********* independently ********
 ********************************/
module.exports = {
    userService: require(`./${CONFIG.PLATFORM}/userService`),
    swaggerService: require(`./${CONFIG.PLATFORM}/swaggerService`),
    authService: require(`./${CONFIG.PLATFORM}/authService`),
    adminService: require(`./${CONFIG.PLATFORM}/adminService`),
    bookingService: require(`./${CONFIG.PLATFORM}/bookingService`),
    fileUploadService: require(`./${CONFIG.PLATFORM}/fileUploadService`)
};