"use strict";
/************* Modules ***********/
const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;
const { USER_ROLES, FUEL_TYPES } = require(`../../utils/constants`);
const { boolean } = require('joi');
/**************************************************
 ************* User Model or collection ***********
 **************************************************/
const userSchema = new Schema({
    name: { type: { first: String, last: String } },
    email: { type: String },
    password: { type: String },
    country: { type: String },
    city: { type: String },
    role: { type: Number, enum: Object.values(USER_ROLES), default: USER_ROLES.USER },
    fuelStationLocation: {
        type: { type: String, enum: ['Point'] },
        coordinates: [{ type: Number, index: '2dsphere' }]
    },
    availableFuels: [{ type: Number }],
    isBlocked: { type: Boolean, default: false },
    isDeleted: { type: Boolean, default: false }
});

userSchema.set('timestamps', true);

module.exports = MONGOOSE.model('user', userSchema);



