"use strict";
/************* Modules ***********/
const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;
const { USER_ROLES, FUEL_TYPES, BOOKING_STATUS } = require(`../../utils/constants`);
/**************************************************
 ************* Booking Model or collection ***********
 **************************************************/
const bookingSchema = new Schema({
    customerId: { type: Schema.Types.ObjectId, ref: 'user' },
    vehicles: [{ vehicleNumber: { type: String, require: true }, fuelType: { type: Number, enum: Object.values(FUEL_TYPES) } }],
    stationId: { type: Schema.Types.ObjectId, ref: 'user' },
    status: { type: Number, enum: Object.values(BOOKING_STATUS), default: BOOKING_STATUS.REQUESTED }
});

bookingSchema.set('timestamps', true);

module.exports = MONGOOSE.model('booking', bookingSchema);



