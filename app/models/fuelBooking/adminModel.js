"use strict";
/************* Modules ***********/
const MONGOOSE = require('mongoose');
const Schema = MONGOOSE.Schema;
/**************************************************
 ************* Admin Model or collection ***********
 **************************************************/
const adminSchema = new Schema({
    email: { type: String, required: true, unique: true, index: true },
    password: { type: String }
});
adminSchema.set('timestamps', true);
module.exports = MONGOOSE.model('admin', adminSchema);